//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser = require('body-parser')
app.use(bodyparser.json())

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var movimientosJSON = require('./movimientosv2.json');

app.get('/', function (req,res) {
  //res.send('Hola Mundo node js');
  res.sendFile(path.join(__dirname,'index.html'));

});

app.get('/Clientes', function (req,res) {
  //res.send('Hola Mundo node js');
  res.send('Aqui estan los clientes devueltos nuevos');

});

app.get('/Clientes/:idCliente', function (req,res) {
  //res.send('Hola Mundo node js');
  res.send('Aqui tiene al cliente numero ' + req.params.idCliente);

});

app.get('/movimientos/v1', function (req,res) {
  //res.send('Hola Mundo node js');
  res.sendfile ('movimientosv1.json');

});

app.get('/movimientos/v2', function (req,res) {
  //res.send('Hola Mundo node js');
  res.sendfile ('movimientosv2.json');

});

app.get('/movimientos/v2/:index', function (req,res) {
  console.log(req.params.index);
  res.send(movimientosJSON[req.params.index-1]);

});

app.get('/movimientosq/v2', function (req,res) {
  console.log(req.query);
  res.send('Recibido')

});


app.post('/', function (req, res) {
  res.send('Hemos recibido su petición post');

});

app.post('/movimientos/v2', function (req, res) {
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  console.log(nuevo);
  res.send('Movimiento dado de alta');

});
